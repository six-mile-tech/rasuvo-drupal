<?php
/**
 * @file
 * rasuvo_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rasuvo_content_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function rasuvo_content_types_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'gallery' => array(
      'name' => t('Gallery'),
      'base' => 'node_content',
      'description' => t('Use this for creating galleries of images'),
      'has_title' => '1',
      'title_label' => t('Galery Title'),
      'help' => t('Make sure you have the copyright for these images.'),
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Page Title'),
      'help' => t('Be careful about spelling'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
